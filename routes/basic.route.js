const express = require('express');
const basicRouter = express.Router();
const People = require('../src/models/people.js');
const peopleController = require('../src/controllers/people.controller');

/*

+--------------------+--------+--------------------------+
| URL                | METHOD | ACTION                   |
+--------------------+--------+--------------------------+
| /people            | GET    | peopleController.findAll |
| /people/new        | GET    | peopleController.new     |
| /people            | POST   | peopleController.create  |
| /people/:id        | GET    | peopleController.show    |
| /people/:id/edit   | GET    | peopleController.edit    |
| /people/:id        | PUT    | peopleController.update  |
| /people/:id        | DELETE | peopleController.delete  |
+--------------------+--------+--------------------------+

*/

basicRouter.get('/people', peopleController.findAll);
basicRouter.get('/people/new', peopleController.new);
basicRouter.post('/people', peopleController.create);
basicRouter.get('/people/:id', peopleController.show);
basicRouter.get('/people/:id/edit', peopleController.edit);
basicRouter.put('/people/:id', peopleController.update);
basicRouter.delete('/people/:id', peopleController.delete);

module.exports = basicRouter;
