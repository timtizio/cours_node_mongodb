const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const basicRouter = require('../routes/basic.route');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
mongoose.connect('mongodb://localhost/node_db');
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('[MongoDB connected !!]');
});

app.use(express.static(__dirname + '/../public'));
app.use(express.urlencoded({extended: false}));

// CUSTOM METHOD PUT, DELETE, ETC...
app.use(bodyParser.urlencoded());
app.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

app.set('view engine', 'pug');
app.use('/', basicRouter);

app.listen(port, function() {
    console.log('[Application is running !!]');
});

