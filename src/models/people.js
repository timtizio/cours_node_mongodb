const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PeopleSchema = new Schema ({
    firstName: {type: String, required: true},
    lastName: String,
    email: String
});

const People = mongoose.model('People', PeopleSchema);

module.exports = People;
