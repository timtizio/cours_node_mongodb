const People = require('../models/people');

module.exports = {
    findAll(req, res) {
        People.find({}, function(err, persons) {
            if(err) 
                res.send(err);

            res.render('about', {persons: persons});
        });
    },
    new(req, res) {
        res.render('new');
    },
    show(req, res) {
        People.findById(req.params.id, function(err, person) {
            if(err)
                res.send(err);
            res.render('person', {person: person});
        });
    },
    create(req, res) {
        const newPeople = new People(req.body);
        console.log(req.body);
        newPeople.save(function(err, person) {
            if(err)
                err.send(err);
            // res.json({message: 'person ajoutée', person});
            res.redirect('/people');
        });
    },
    findOne(req, res) {
        People.findById(req.params.id, function(err, person) {
            if(err)
                res.send(err);
            res.json(person);
        });
    },
    edit(req, res) {
        People.findById({_id: req.params.id}, function(err, person) {
            res.render('edit', {person: person});
        });
    },
    update(req, res) {
        People.findById({_id: req.params.id}, function(err, person) {
            if(err)
                res.send(err);
            console.log(req.body);
            Object.assign(person, req.body).save(function(err, person) {
                if(err)
                    res.send(err);
                // res.json({message: 'personne modifiée', person});
                res.redirect('/people');
            });
        });
    },
    delete(req, res) {
        People.findOneAndDelete({_id: req.params.id}, function(err, person) {
            if(err)
                res.send(err);
            res.redirect('/people');
        });
    }
}
